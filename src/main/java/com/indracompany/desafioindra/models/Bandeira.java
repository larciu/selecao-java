package com.indracompany.desafioindra.models;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Bandeira {
	@Id
	@GeneratedValue
	private long id;
	private String nome;
	private String status;
	
	public Bandeira atualizarBandeira (Bandeira bandeira) {
		this.nome = bandeira.getNome();
		this.status = bandeira.getStatus();
		
		return this;
	}
}
