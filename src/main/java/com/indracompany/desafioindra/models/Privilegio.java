package com.indracompany.desafioindra.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Privilegio {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String nome;
	private String status;
	
	public Privilegio atualizar(Privilegio privilegio) {
		this.nome = privilegio.getNome();
		this.status = privilegio.getStatus();
		
		return this;
	}

}
