package com.indracompany.desafioindra.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Posto {
	
	@Id
	@GeneratedValue
	private long id;
	private String nome;
	private String status;
	
	public Posto atualizarPosto(Posto posto) {
		this.nome = posto.getNome();
		this.status = posto.getStatus();
		return this;
	}
}
