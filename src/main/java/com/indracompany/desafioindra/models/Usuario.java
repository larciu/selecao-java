package com.indracompany.desafioindra.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@AllArgsConstructor @NoArgsConstructor @Setter @Getter
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long id_privilege;
	private String nome;
	private String email;
	private String senha;
	private String status;
	
	public Usuario atualizar(Usuario usuario) {
		this.nome = usuario.getNome();
		this.id_privilege = usuario.getId_privilege();
		this.email = usuario.getEmail();
		this.senha = usuario.getSenha();
		this.status = usuario.getStatus();
		
		return this;
	}
}
