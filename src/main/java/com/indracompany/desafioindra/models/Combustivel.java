package com.indracompany.desafioindra.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@Entity
public class Combustivel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String nome;
	private String unidadeMedia;
	private String status;
	
	public Combustivel atualizarCombustivel(Combustivel combustivel) {
		this.nome = combustivel.getNome();
		this.status = combustivel.getStatus();
		this.unidadeMedia = combustivel.getUnidadeMedia();
		
		return this;
	}
}
