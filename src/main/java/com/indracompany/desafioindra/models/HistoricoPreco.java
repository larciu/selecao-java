package com.indracompany.desafioindra.models;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class HistoricoPreco {
	@Id
	@GeneratedValue
	private long id;
	private long id_combustivel;
	private long id_posto;
	private long id_bandeira;
	private String data_coleta;
	private double valorVenda;
	private double valorCompra;
	private String regiao;
	private String estado;
	private String municipio;
	
	public HistoricoPreco atualizarHistoricoPreco(HistoricoPreco historicoPreco) {
		this.id_combustivel = historicoPreco.getId_combustivel();
		this.id_posto = historicoPreco.getId_posto();
		this.id_bandeira = historicoPreco.getId_bandeira();
		this.data_coleta = historicoPreco.getData_coleta();
		this.valorVenda = historicoPreco.getValorVenda();
		this.valorCompra = historicoPreco.getValorCompra();
		this.regiao = historicoPreco.getRegiao();
		this.estado = historicoPreco.getEstado();
		this.municipio = historicoPreco.getMunicipio();
		
		return this;
	}

}
