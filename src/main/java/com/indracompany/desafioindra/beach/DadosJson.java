package com.indracompany.desafioindra.beach;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DadosJson {
	private String combustivel;
	private String  posto;
	private String bandeira;
	private String data_coleta;
	private double valor_venda;
	private double valor_compra;
	private String regiao;
	private String estado;
	private String municipio;
	
	public DadosJson converterObject(Object[] object) {
		this.setCombustivel((String)object[0]);
		this.setPosto((String) object[1]);
		this.setBandeira((String) object[2]);
		this.setData_coleta((String)object[3]);
		this.setValor_venda((double)object[4]);
		this.setValor_compra((double)object[5]);
		this.setRegiao((String)object[6]);
		this.setEstado((String) object[7]);
		this.setMunicipio((String)object[8]);
		
		return this;
	} 
}
