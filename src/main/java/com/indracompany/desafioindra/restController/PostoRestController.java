package com.indracompany.desafioindra.restController;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.desafioindra.dao.PostoRepositorio;
import com.indracompany.desafioindra.models.Combustivel;
import com.indracompany.desafioindra.models.Posto;

import io.swagger.annotations.ApiOperation;

@RestController
public class PostoRestController {
	@Autowired
	private PostoRepositorio postoRepositorio;
	@ApiOperation(value = "Listar um posto específico")
	@RequestMapping(value = "/listar-posto/{id}",method = RequestMethod.GET)
	public ResponseEntity<Object> listarPosto(@PathVariable("id") long id){
		Optional<Posto> posto = this.postoRepositorio.findById(id);
		
		if(posto.isPresent()) {
			return new ResponseEntity<>(posto.get(),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Posto não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Cadastrar-posto")
	@RequestMapping(value="/cadastrar-posto",method = RequestMethod.POST)
	public ResponseEntity<Object> cadastrarPosto(Posto posto){
		Posto postoRetorno = this.postoRepositorio.save(posto);
		if(postoRetorno == null) {
			return new ResponseEntity<>("Não foi possível o cadastro,por favor tente novamente",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else {
			return new ResponseEntity<>(postoRetorno, HttpStatus.OK);
		}
	}
	@ApiOperation(value = "Atualizar um posto específico")
	@RequestMapping(value="/atualizar-posto/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> atualizarPosto(@PathVariable("id") long id, Posto posto){
		Optional<Posto> postoRetorno = this.postoRepositorio.findById(id);
		
		if(postoRetorno.isPresent()) {
			Posto postoAtual = postoRetorno.get();
			postoAtual = postoAtual.atualizarPosto(posto);
			
			this.postoRepositorio.save(postoAtual);
			
			return new ResponseEntity<>(postoAtual,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Combustivel não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Deletar um posto específico")
	@RequestMapping(value = "/deletar-posto/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarPosto(@PathVariable("id")long id){
		Optional<Posto> postoRetorno = this.postoRepositorio.findById(id);
		
		if(postoRetorno.isPresent()) {
			this.postoRepositorio.deleteById(id);
			return new ResponseEntity<>("Posto Deletado com sucesso",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Posto não encontrado!",HttpStatus.NOT_FOUND);
		}
	}
}
