package com.indracompany.desafioindra.restController;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.desafioindra.dao.UsuarioRepositorio;
import com.indracompany.desafioindra.models.Usuario;

import io.swagger.annotations.ApiOperation;

@RestController
public class UsuarioRestController {
	@Autowired
	private UsuarioRepositorio usuarioRepositorio;
	@ApiOperation(value = "Cadastar um usuário")
	@RequestMapping(value = "/cadastrar-usuario", method = RequestMethod.POST)
	public ResponseEntity<Object> cadastrarUsuario(Usuario usuario) {
		Usuario usuarioRetorno =  this.usuarioRepositorio.save(usuario);
		if(usuarioRetorno == null) {
			return new ResponseEntity<>("Não foi possível o cadastro, por favor tente novamente.",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(usuarioRetorno,HttpStatus.OK);
	}
	@ApiOperation(value = "Listar um usuário específico")
	@RequestMapping(value = "/listar-usuario/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> listarUsuario(@PathVariable(value = "id") long id) {
		Optional<Usuario> usuarioRetorno = this.usuarioRepositorio.findById(id);
		if(usuarioRetorno.isPresent()) {
			return new ResponseEntity<>(usuarioRetorno.get(), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Usuário não encontrado",HttpStatus.NOT_FOUND);
		}
		
	}
	@ApiOperation(value = "Deletar um usuário específico")
	@RequestMapping(value="/deletar-usuario/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarUsuario(@PathVariable("id") long id){
		Optional<Usuario> usuarioRetorno = this.usuarioRepositorio.findById(id);
		if(usuarioRetorno.isPresent()) {
			this.usuarioRepositorio.deleteById(id);
			return new ResponseEntity<>("Usuário excluído com sucesso!",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Usuário não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Atualizar um usuário específico")
	@RequestMapping(value = "/atualizar-usuario/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> AtualizarUsuario(@PathVariable("id") long id, Usuario usuarioNovo){
		Optional<Usuario> usuarioRetorno = this.usuarioRepositorio.findById(id);
		if(usuarioRetorno.isPresent()) {
			Usuario usuarioAtual = usuarioRetorno.get();
			usuarioAtual = usuarioAtual.atualizar(usuarioNovo);
			this.usuarioRepositorio.save(usuarioAtual);
			return new ResponseEntity<>(usuarioAtual,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Usuário não encontrado",HttpStatus.NOT_FOUND);
		}
	}
}
