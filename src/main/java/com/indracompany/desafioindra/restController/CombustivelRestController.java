package com.indracompany.desafioindra.restController;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.desafioindra.dao.CombustivelRepositorio;
import com.indracompany.desafioindra.models.Combustivel;
import com.indracompany.desafioindra.models.HistoricoPreco;

import io.swagger.annotations.ApiOperation;

@RestController
public class CombustivelRestController {
	
	@Autowired
	private CombustivelRepositorio combustivelRepositorio;
	@ApiOperation(value = "Listar um combustível específico")
	@RequestMapping(value = "/listar-combustivel/{id}",method = RequestMethod.GET)
	public ResponseEntity<Object> listarCombustivel(@PathVariable("id") long id){
		Optional<Combustivel> combustivel = this.combustivelRepositorio.findById(id);
		
		if(combustivel.isPresent()) {
			return new ResponseEntity<>(combustivel.get(),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Combustível não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Cadastrar combustível")
	@RequestMapping(value="/cadastrar-combustivel",method = RequestMethod.POST)
	public ResponseEntity<Object> cadastrarCombustivel(Combustivel combustivel){
		Combustivel combustivelRetorno = this.combustivelRepositorio.save(combustivel);
		if(combustivelRetorno == null) {
			return new ResponseEntity<>("Não foi possível o cadastro,por favor tente novamente",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else {
			return new ResponseEntity<>(combustivelRetorno, HttpStatus.OK);
		}
	}
	@ApiOperation(value = "Atualizar Combustível")
	@RequestMapping(value="/atualizar-combustivel/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> atualizarHistorico(@PathVariable("id") long id, Combustivel combustivel){
		Optional<Combustivel> combustivelRetorno = this.combustivelRepositorio.findById(id);
		
		if(combustivelRetorno.isPresent()) {
			Combustivel combustivelAtual = combustivelRetorno.get();
			combustivelAtual = combustivelAtual.atualizarCombustivel(combustivel);
			
			this.combustivelRepositorio.save(combustivelAtual);
			
			return new ResponseEntity<>(combustivelAtual,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Combustivel não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Deletar combustível")
	@RequestMapping(value = "/deletar-combustivel/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarCombustivel(@PathVariable("id")long id){
		Optional<Combustivel> combustivelRetorno = this.combustivelRepositorio.findById(id);
		
		if(combustivelRetorno.isPresent()) {
			this.combustivelRepositorio.deleteById(id);
			return new ResponseEntity<>("Combustível Deletado com sucesso",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Combustível não encontrado!",HttpStatus.NOT_FOUND);
		}
	}
}
