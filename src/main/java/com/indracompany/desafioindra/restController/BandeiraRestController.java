package com.indracompany.desafioindra.restController;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.desafioindra.dao.BandeiraRepositorio;
import com.indracompany.desafioindra.models.Bandeira;
import com.indracompany.desafioindra.models.Posto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Crud da bandeira")
public class BandeiraRestController {
	@Autowired
	private BandeiraRepositorio bandeiraRepositorio;
	@ApiOperation(value = "Listar uma bandeira específica")
	@RequestMapping(value = "/listar-bandeira/{id}",method = RequestMethod.GET)
	public ResponseEntity<Object> listarBandeira(@PathVariable("id") long id){
		Optional<Bandeira> bandeira = this.bandeiraRepositorio.findById(id);
		
		if(bandeira.isPresent()) {
			return new ResponseEntity<>(bandeira.get(),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Bandeira não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "cadastrar uma bandeira específica")
	@RequestMapping(value="/cadastrar-bandeira",method = RequestMethod.POST)
	public ResponseEntity<Object> cadastrarBandeira(Bandeira bandeira){
		Bandeira bandeiraRetorno = this.bandeiraRepositorio.save(bandeira);
		if(bandeiraRetorno == null) {
			return new ResponseEntity<>("Não foi possível o cadastro,por favor tente novamente",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else {
			return new ResponseEntity<>(bandeiraRetorno, HttpStatus.OK);
		}
	}
	@ApiOperation(value = "Atualizar uma bandeira específica")
	@RequestMapping(value="/atualizar-bandeira/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> atualizarBandeira(@PathVariable("id") long id, Bandeira bandeira){
		Optional<Bandeira> bandeiraRetorno = this.bandeiraRepositorio.findById(id);
		if(bandeiraRetorno.isPresent()) {
			Bandeira bandeiraAtual = bandeiraRetorno.get();
			bandeiraAtual = bandeiraAtual.atualizarBandeira(bandeira);
			
			this.bandeiraRepositorio.save(bandeiraAtual);
			
			return new ResponseEntity<>(bandeiraAtual,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Bandeira não encontrada",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Deletar uma bandeira específica")
	@RequestMapping(value = "/deletar-bandeira/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarBandeira(@PathVariable("id")long id){
		Optional<Bandeira> bandeiraRetorno = this.bandeiraRepositorio.findById(id);
		
		if(bandeiraRetorno.isPresent()) {
			this.bandeiraRepositorio.deleteById(id);
			return new ResponseEntity<>("Bandeira Deletada com sucesso",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Bandeira não encontrada!",HttpStatus.NOT_FOUND);
		}
	}
}
