package com.indracompany.desafioindra.restController;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.desafioindra.dao.PrivilegioRepositorio;
import com.indracompany.desafioindra.models.Privilegio;

import io.swagger.annotations.ApiOperation;

@RestController
public class PrivilegioRestController {
	@Autowired
	private PrivilegioRepositorio privilegioRepositorio;
	@ApiOperation(value = "Listar um privilégio específico")
	@RequestMapping(value = "/listar-privilegio/{id}",method = RequestMethod.GET)
	public ResponseEntity<Object> listarPrivilegio(@PathVariable("id") long id){
		Optional<Privilegio> privilegio = this.privilegioRepositorio.findById(id);
		
		if(privilegio.isPresent()) {
			return new ResponseEntity<>(privilegio.get(),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Privilégio não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Cadastrar privilégio")
	@RequestMapping(value="/cadastrar-privilegio",method = RequestMethod.POST)
	public ResponseEntity<Object> cadastrarPrivilegio(Privilegio privilegio){
		Privilegio privilegioRetorno = this.privilegioRepositorio.save(privilegio);
		if(privilegioRetorno == null) {
			return new ResponseEntity<>("Não foi possível o cadastro,, por favor tente novamente",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else {
			return new ResponseEntity<>(privilegioRetorno, HttpStatus.OK);
		}
	}
	@ApiOperation(value = "Atualizar um privilégio específico")
	@RequestMapping(value="/atualizar-privilegio/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> atualizarPrivilegio(@PathVariable("id") long id, Privilegio novoPrivilegio){
		Optional<Privilegio> privilegioRetorno = this.privilegioRepositorio.findById(id);
		
		if(privilegioRetorno.isPresent()) {
			Privilegio privilegioAtual = privilegioRetorno.get();
			privilegioAtual = privilegioAtual.atualizar(novoPrivilegio);
			
			this.privilegioRepositorio.save(privilegioAtual);
			
			return new ResponseEntity<>(privilegioAtual,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Privilegio não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Deletar um privilégio específico")
	@RequestMapping(value = "/deletar-privilegio/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarPrivilegio(@PathVariable("id")long id){
		Optional<Privilegio> privilegioRetorno = this.privilegioRepositorio.findById(id);
		
		if(privilegioRetorno.isPresent()) {
			this.privilegioRepositorio.deleteById(id);
			return new ResponseEntity<>("Privilégio Deletado com sucesso",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Usuário não encontrado!",HttpStatus.NOT_FOUND);
		}
	}
}
