package com.indracompany.desafioindra.restController;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.indracompany.desafioindra.beach.DadosJson;
import com.indracompany.desafioindra.beach.MediaGasolina;
import com.indracompany.desafioindra.dao.HistoricoPrecoRepositorio;
import com.indracompany.desafioindra.models.HistoricoPreco;
import com.indracompany.desafioindra.upload.UploadCsv;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Crud do histórico de preços")
public class HistoricoPrecoRestController {
	@Autowired
	private HistoricoPrecoRepositorio historicoPrecoRepositorio;
	
	@Autowired
	private UploadCsv uploadCsv;
	@ApiOperation(value = "Listar um histórico específico")
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Não foi possível importar as informações. Por favor tente novamente")
	@RequestMapping(value = "/listar-historico/{id}",method = RequestMethod.GET)
	public ResponseEntity<Object> listarHistorico(@PathVariable("id") long id){
		Optional<HistoricoPreco> historicoPreco = this.historicoPrecoRepositorio.findById(id);
		
		if(historicoPreco.isPresent()) {
			return new ResponseEntity<>(historicoPreco.get(),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Histórico de preço não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Cadastrar um histórico")
	@RequestMapping(value="/cadastrar-historico",method = RequestMethod.POST)
	public ResponseEntity<Object> cadastrarHistorico(HistoricoPreco historico){
		HistoricoPreco historicoRetorno = this.historicoPrecoRepositorio.save(historico);
		if(historicoRetorno == null) {
			return new ResponseEntity<>("Não foi possível o cadastro,por favor tente novamente",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else {
			return new ResponseEntity<>(historicoRetorno, HttpStatus.OK);
		}
	}
	@ApiOperation(value = "Atualizar um histórico")
	@RequestMapping(value="/atualizar-historico/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> atualizarHistorico(@PathVariable("id") long id, HistoricoPreco historico){
		Optional<HistoricoPreco> historicoRetorno = this.historicoPrecoRepositorio.findById(id);
		
		if(historicoRetorno.isPresent()) {
			HistoricoPreco historicoAtual = historicoRetorno.get();
			historicoAtual = historicoAtual.atualizarHistoricoPreco(historico);
			
			this.historicoPrecoRepositorio.save(historicoAtual);
			
			return new ResponseEntity<>(historicoAtual,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Histórico não encontrado",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Deletar um histórico")
	@RequestMapping(value = "/deletar-historico/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarHistorico(@PathVariable("id")long id){
		Optional<HistoricoPreco> historicoRetorno = this.historicoPrecoRepositorio.findById(id);
		
		if(historicoRetorno.isPresent()) {
			this.historicoPrecoRepositorio.deleteById(id);
			return new ResponseEntity<>("Histórico Deletado com sucesso",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Histórico não encontrado!",HttpStatus.NOT_FOUND);
		}
	}
	@ApiOperation(value = "Listar todos os históricos")
	@RequestMapping(value = "/listar-historico-total", method = RequestMethod.GET)
	public ResponseEntity<Object> listarUsuariosTotal(){
		List<HistoricoPreco> historicoPrecoRetorno =  this.historicoPrecoRepositorio.findAll();
		
		if(historicoPrecoRetorno.isEmpty()) {
			return new ResponseEntity<>("A lista de usuários está vazia",HttpStatus.NOT_FOUND);
		}
		else {
			return new ResponseEntity<>(historicoPrecoRetorno, HttpStatus.OK);
		}
	}
	@ApiOperation(value = "Média de preço por municipio")
	@RequestMapping(value = "/media-preco-combustivel", method = RequestMethod.GET)
	public ResponseEntity<Object> mediaPreco(){
		List<Object> mediaPreco = this.historicoPrecoRepositorio.mediaPreco();
		List<MediaGasolina> mediaGasolinas = new ArrayList<MediaGasolina>();
		Object[] object;
		MediaGasolina mediaGasolina;
		for (int i = 0; i < mediaPreco.size(); i++) {
			object = (Object[]) mediaPreco.get(i);
			mediaGasolina = new MediaGasolina();
			mediaGasolina.setMedia((double)object[0]);
			mediaGasolina.setMunicipio((String)object[1]);
			mediaGasolinas.add(mediaGasolina);
		}
		return new ResponseEntity<>(mediaGasolinas, HttpStatus.OK);
	}
	@ApiOperation(value = "Agrupamento de todos os dados por Sigla da UF")
	@RequestMapping(value="/sigla-regiao" ,method = RequestMethod.GET)
	public ResponseEntity<Object> siglaRegiao(){
		List<Object> siglaRegiao = this.historicoPrecoRepositorio.siglaRegiao();
		List<DadosJson> siglaRegiaoAux = new ArrayList<DadosJson>();
		Object[] object;
		for (int i = 0; i < siglaRegiao.size(); i++) {
			object = (Object[]) siglaRegiao.get(i);
			DadosJson siglaRegioes = new DadosJson();
			siglaRegioes = siglaRegioes.converterObject(object);
			siglaRegiaoAux.add(siglaRegioes);
		}
		return new ResponseEntity<>(siglaRegiaoAux, HttpStatus.OK);
		
	}
	@ApiOperation(value = "Agrupamento de dados por data de coleta")
	@RequestMapping(value="/dados-coleta" ,method = RequestMethod.GET)
	public ResponseEntity<Object> dataColeta(){
		List<Object> siglaRegiao = this.historicoPrecoRepositorio.dataColeta();
		List<DadosJson> siglaRegiaoAux = new ArrayList<DadosJson>();
		Object[] object;
		for (int i = 0; i < siglaRegiao.size(); i++) {
			object = (Object[]) siglaRegiao.get(i);
			DadosJson siglaRegioes = new DadosJson();
			siglaRegioes = siglaRegioes.converterObject(object);
			siglaRegiaoAux.add(siglaRegioes);
		}
		return new ResponseEntity<>(siglaRegiaoAux, HttpStatus.OK);
		
	}
	@ApiOperation(value = "Agrupamento de dados por distribuidora")
	@RequestMapping(value="/dados-distribuidora" ,method = RequestMethod.GET)
	public ResponseEntity<Object> distribuidora(){
		List<Object> siglaRegiao = this.historicoPrecoRepositorio.distribuidora();
		List<DadosJson> siglaRegiaoAux = new ArrayList<DadosJson>();
		Object[] object;
		for (int i = 0; i < siglaRegiao.size(); i++) {
			object = (Object[]) siglaRegiao.get(i);
			DadosJson siglaRegioes = new DadosJson();
			siglaRegioes = siglaRegioes.converterObject(object);
			siglaRegiaoAux.add(siglaRegioes);
		}
		return new ResponseEntity<>(siglaRegiaoAux, HttpStatus.OK);
		
	}
	@ApiOperation(value = "Importar CSV")
	@RequestMapping(value = "/upload-csv", method = RequestMethod.POST)
	public ResponseEntity<Object> importandoCsv(@RequestParam("csv") MultipartFile csv)
	{
		boolean path = false;
		try {
			path = this.uploadCsv.uploadCsv(csv);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(path) {
			return new ResponseEntity<>("dados importados com sucesso",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Erro ao importar.",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
