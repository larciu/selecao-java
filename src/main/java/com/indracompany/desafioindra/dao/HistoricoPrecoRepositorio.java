package com.indracompany.desafioindra.dao;

import java.util.List;

import javax.print.attribute.standard.Media;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.indracompany.desafioindra.beach.MediaGasolina;
import com.indracompany.desafioindra.models.HistoricoPreco;

public interface HistoricoPrecoRepositorio extends JpaRepository<HistoricoPreco, Long> {
	@Query(value = "select avg(valor_venda), municipio from historico_preco group by municipio", nativeQuery = true)
	List<Object> mediaPreco();
	
	@Query(value = "select combustivel.nome as combustivel, posto.nome as posto, bandeira.nome as bandeira, historico_preco.data_coleta, valor_venda, valor_compra, regiao, estado, municipio from historico_preco left join bandeira on bandeira.id = historico_preco.id_bandeira left join posto on posto.id = historico_preco.id_posto left join combustivel on combustivel.id = historico_preco.id_combustivel group by estado, combustivel.nome, bandeira.nome, posto.nome", nativeQuery = true)
	List<Object> siglaRegiao();
	
	@Query(value = "select combustivel.nome as combustivel, posto.nome as posto, bandeira.nome as bandeira, historico_preco.data_coleta, valor_venda, valor_compra, regiao, estado, municipio from historico_preco left join bandeira on bandeira.id = historico_preco.id_bandeira left join posto on posto.id = historico_preco.id_posto left join combustivel on combustivel.id = historico_preco.id_combustivel group by data_coleta, combustivel.nome, bandeira.nome, posto.nome", nativeQuery = true)
	List<Object> dataColeta();
	
	@Query(value = "select combustivel.nome as combustivel, posto.nome as posto, bandeira.nome as bandeira, historico_preco.data_coleta, valor_venda, valor_compra, regiao, estado, municipio from historico_preco left join bandeira on bandeira.id = historico_preco.id_bandeira left join posto on posto.id = historico_preco.id_posto left join combustivel on combustivel.id = historico_preco.id_combustivel group by bandeira.nome, combustivel.nome, bandeira.nome, posto.nome", nativeQuery = true)
	List<Object> distribuidora();
}
