package com.indracompany.desafioindra.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.desafioindra.models.Privilegio;

public interface PrivilegioRepositorio extends JpaRepository<Privilegio, Long> {
	
}
