package com.indracompany.desafioindra.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.desafioindra.models.Combustivel;

public interface CombustivelRepositorio extends JpaRepository<Combustivel, Long> {

}
