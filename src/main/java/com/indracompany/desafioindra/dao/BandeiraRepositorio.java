package com.indracompany.desafioindra.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.desafioindra.models.Bandeira;

public interface BandeiraRepositorio extends JpaRepository<Bandeira, Long> {

}
