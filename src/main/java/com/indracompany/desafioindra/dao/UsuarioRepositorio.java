package com.indracompany.desafioindra.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.desafioindra.models.Usuario;

public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {
	
}
