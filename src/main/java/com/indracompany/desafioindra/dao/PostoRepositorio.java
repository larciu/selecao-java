package com.indracompany.desafioindra.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.desafioindra.models.Posto;

public interface PostoRepositorio extends JpaRepository<Posto, Long> {

}
