package com.indracompany.desafioindra;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
          .select()
          .apis(RequestHandlerSelectors.any())
          .paths(PathSelectors.any())
          .build();
    }
	
	private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfo(
                "API Pesquisa Combustível",
                "API REST de pesquisa de combustível",
                "2.0",
                "Terms of Service",
                new Contact("Láercio da Silva Pedrosa", "https://www.linkedin.com/in/la%C3%A9rcio-da-silva-pedrosa-9a13a4154/",
                        "laerciodasilvapedrosa@gmail.com"),
                "Apache License Version 2.0",
                "https://www.apache.org/licesen.html", new ArrayList<VendorExtension>()
        );

        return apiInfo;
    }
}
