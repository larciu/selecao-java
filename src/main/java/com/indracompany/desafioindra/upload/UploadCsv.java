package com.indracompany.desafioindra.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.indracompany.desafioindra.dao.HistoricoPrecoRepositorio;
import com.indracompany.desafioindra.models.HistoricoPreco;
@Component
public class UploadCsv {
	@Autowired
	private HistoricoPrecoRepositorio historicoPrecoRepositorio;
	public boolean uploadCsv(MultipartFile file) throws FileNotFoundException {
		String nomeArquivo = file.getOriginalFilename();
		String extensao = nomeArquivo.substring(nomeArquivo.length()-3);
		if(!extensao.equalsIgnoreCase("csv") || file.isEmpty()) {
			return false;
		}
		String nomeUnico = String.valueOf(System.currentTimeMillis());
		Path pathCsv = Paths.get(System.getProperty("user.dir")+"/uploads");
		Path arquiPath = pathCsv.resolve(nomeUnico+file.getOriginalFilename());
		File arquivoCsv = new File(arquiPath.toString());
		String linhasDoArquivo = "";
		
		try {
			Files.createDirectories(pathCsv);
			file.transferTo(arquiPath.toFile());
			Scanner leitor = new Scanner(new FileInputStream(arquiPath.toFile()),"UTF-8");
			leitor.nextLine();
			while(leitor.hasNext()) {
				linhasDoArquivo = leitor.nextLine();
				String[] valoresEntreVirgulas = linhasDoArquivo.split(";");
				HistoricoPreco historicoPreco = new HistoricoPreco();
				historicoPreco.setId_combustivel(Long.parseLong(valoresEntreVirgulas[0]));
				historicoPreco.setId_posto(Long.parseLong(valoresEntreVirgulas[1]));
				historicoPreco.setId_bandeira(Long.parseLong(valoresEntreVirgulas[2]));
				historicoPreco.setData_coleta(valoresEntreVirgulas[3]);
				historicoPreco.setValorVenda(Long.parseLong(valoresEntreVirgulas[4]));
				historicoPreco.setValorCompra(Long.parseLong(valoresEntreVirgulas[5]));
				historicoPreco.setMunicipio(valoresEntreVirgulas[6]);
				historicoPreco.setEstado(valoresEntreVirgulas[7]);
				historicoPreco.setRegiao(valoresEntreVirgulas[8]);
				
				this.historicoPrecoRepositorio.save(historicoPreco);
				
			}
		}
		catch(IOException | NumberFormatException e) {
			return false;
		}
		
		
		return true;
	}
}
